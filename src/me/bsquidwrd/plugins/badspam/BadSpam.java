package me.bsquidwrd.plugins.badspam;

import java.io.IOException;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

public class BadSpam extends JavaPlugin implements Listener {

	public boolean badSpamEnable;
	public String lastMessage;
	public BukkitTask task;
	public JavaPlugin plugin = this;

	public String errorMessage;
	public int chatInterval;

	public void onEnable() {
		plugin.getServer().getPluginManager().registerEvents(this, plugin);

		try {
			Metrics metrics = new Metrics(this);
			metrics.start();
		} catch (IOException e) {
			// Failed to submit the stats :-(
		}

		this.saveDefaultConfig();
		errorMessage = this.getConfig().getString("message");
		chatInterval = this.getConfig().getInt("interval");
		if(chatInterval == 0) {
			chatInterval = 10;
		}

	}

	public void onDisable() {

	}

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {

		if(!((sender instanceof Player))) {
			sender.sendMessage(ChatColor.RED + "You must be a player to use these commands!");
			return true;
		}

		readCommand((Player)sender, commandLabel, args);
		return false;
	}

	public void readCommand(Player player, String command, String[] args) {
		if (player != null) {

			if(command.equalsIgnoreCase("badspam")) {
				player.sendMessage(ChatColor.AQUA + "[BadSpam] Created by: bsquidwrd");
				player.sendMessage(ChatColor.AQUA + "[BadSpam] Version: " + this.getDescription().getVersion());
				player.sendMessage(ChatColor.AQUA + "[BadSpam] Interval: " + chatInterval + " seconds");
				player.sendMessage(ChatColor.AQUA + "[BadSpam] Error Message: ");
				player.sendMessage(ChatColor.RED + getConfig().getString("message"));
			}

		} else {
			System.out.println("[BadSpam] Error: Who is this 'null' guy, and why is he trying to execute a command?");
		}
	}

	public void noPerms(Player player) {
		player.sendMessage(ChatColor.AQUA + "[BadSpam] " + ChatColor.RED + "You do not have permission to do that");
	}

	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		if(!player.hasPermission("badspam.exempt")) {
			String newMessage = event.getMessage().toString();
			if(!newMessage.equalsIgnoreCase(lastMessage) || lastMessage == null) {
				if(task != null) {
					task.cancel();
				}
				lastMessage = newMessage;
				new BadSpamTask(this).runTaskLater(plugin, (chatInterval * 20));
			} else {
				event.setCancelled(true);
				if(errorMessage == null) {
					player.sendMessage(ChatColor.RED + "[BadSpam] Please wait 10 seconds before trying to send the same message!");
				} else {
					player.sendMessage(ChatColor.RED + "[BadSpam] " + errorMessage);
				}
			}
		}
	}

	class BadSpamTask extends BukkitRunnable {
		public BadSpamTask (JavaPlugin plugin) {

		}

		@Override
		public void run() {
			lastMessage = null;

		}

	}

	//yup yup yup ducky, that's my name

}
